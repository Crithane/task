<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
    protected $dates = [
        'lastbirthday'
    ];

    protected $fillable = [
        'lastbirthday',
        'rate'
    ];

    function getDateAttribute() {
        return $this->lastbirthday->format('jS F Y');
    }
}
