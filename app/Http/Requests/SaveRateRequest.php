<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Fideloper\Proxy\TrustedProxyServiceProvider;

class SaveRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dateInput' => 'required|regex:/[0-9][0-9]-[0-9][0-9]/'
        ];
    }
}
