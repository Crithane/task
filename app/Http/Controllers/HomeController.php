<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\FixerService;

use App\ExchangeRate;

use Carbon\Carbon;
use App\Http\Requests\SaveRateRequest;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exchangeRates = ExchangeRate::orderBy('lastbirthday', 'desc')->get();

        return view('home', compact('exchangeRates'));
    }

    /**
     * Fetches EUR->GBP exchange rate from most recent birthday.
     * 
     * @throws Exception
     *
     * @return mixed
     */
    public function saveRate(SaveRateRequest $request)
    {
        $carbonNow = Carbon::now();

        $lastBirthday = Carbon::parse($request->input('dateInput') . "-" . $carbonNow->year);

        if ($lastBirthday > $carbonNow) {
            $lastBirthday = $lastBirthday->subYear(1);
        }

        $lastBirthday = $lastBirthday->format('Y-m-d');
        $carbonNow = $carbonNow->format('Y-m-d');

        try {
            $fixer = new FixerService;

            if ($lastBirthday == $carbonNow) {
                $content = $fixer->getLatestExchangeRate(["GBP"]);
            } else {
                $content = $fixer->getHistoricalExchangeRate($lastBirthday, ["GBP"]);
            }

            $contentArray = json_decode($content, true);

            $existingEntry = ExchangeRate::wherelastbirthday($lastBirthday)->first();
            if ($existingEntry) {
                $existingEntry->increment('count');
                $existingEntry->save();
            } else {

                ExchangeRate::create([
                    'lastbirthday' => $lastBirthday,
                    'rate' => $contentArray['rates']['GBP']
                ]);
            }
            return back()->with('success', 'Exchange rate fetched successfully.');
        } catch (\Exception $e) {
            return back()->with('danger', 'Unfortunately, this service is down, try again later');
        }
    }
}
