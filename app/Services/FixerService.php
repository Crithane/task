<?php

namespace App\Services;

use GuzzleHttp\Client;
use Carbon\Carbon;

class FixerService {

    private $fixerApiKey;
    private $guzzleClient;

    const API_URL = 'http://data.fixer.io/api/';


    public function __construct()
    {
        $this->fixerApiKey = env('FIXER_API_KEY');
        $this->guzzleClient = new Client();
    }

    public function getHistoricalExchangeRate(String $date, Array $symbols)
    {
        $symbols = implode(',', $symbols);

        $response = $this->guzzleClient->request('GET', self::API_URL . $date,
            [
                'query' => [
                    'access_key' => $this->fixerApiKey,
                    'symbols' => $symbols
                ]
            ]
        );

        return $response->getBody()->getContents();
    }

    public function getLatestExchangeRate(array $symbols)
    {
        $symbols = implode(',', $symbols);

        $response = $this->guzzleClient->request(
            'GET',
            self::API_URL . 'latest',
            [
                'query' => [
                    'access_key' => $this->fixerApiKey,
                    'symbols' => $symbols
                ]
            ]
        );

        return $response->getBody()->getContents();
    }
}