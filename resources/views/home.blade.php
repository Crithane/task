@extends('layouts.app')

@section('content')
<div class="container">
    @if(session('success'))
        <div class="col-md-12">
            <div class="alert alert-success text-center" role="alert">
                {{ session('success') }}
            </div>
        </div>
    @endif
    @if(session('error'))
        <div class="col-md-12">
            <div class="alert alert-danger text-center" role="alert">
                {{ session('error') }}
            </div>
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Birthday Exchange Rates</div>
                <div class="card-body">
                    @if ($errors->has('dateInput'))
                        <div class="alert alert-danger text-center" role="alert">
                            <strong>{{ $errors->first('dateInput')}}</strong>
                        </div>
                    @endif
                    <form method="POST" action="{{route('saveRate')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="dateInput">Enter your birth day and month in the following format dd-mm</label>
                            <input type="text" class="form-control" id="dateInput" name="dateInput" placeholder="Enter date" autocomplete="off">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">Birthday</th>
                        <th scope="col">Exchange Rate (EUR -> GBP)</th>
                        <th scope="col">Count</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($exchangeRates as $exhangeRate)
                        <tr>
                        <td>{{ $exhangeRate->date }}</td>
                        <td>{{ $exhangeRate->rate }}</td>
                        <td>{{ $exhangeRate->count }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function() {
        $( "#dateInput" ).datepicker({
            dateFormat: 'dd-mm'
        });
    });
</script>
@endpush